<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki
 */
class ProjectWikiFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_entity_content',
    'project_wiki_markdown_content',
    'project_wiki_markdown_content_example',
    'text',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests multiple project_wiki content type instances rendering correctly.
   *
   * Checks, whether having instances of both project wiki content types
   * ("entity_content" and "markdown_content") are being rendered correctly on
   * the project wiki page.
   */
  public function testMultiplePluginInstancesRenderedCorrectly() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Create a new test entry as entity content.
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(200);
    $page->fillField('edit-category-0-value', 'Test Category');
    $page->fillField('edit-title-0-value', 'Test Entity Content');
    $page->fillField('edit-body-0-value', 'This entry is a test.');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Go to the project wiki list page and make sure that both the entity
    // and the markdown contents from the
    // "project_wiki_markdown_content_example" Module are present:
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Test Entity Content');
    $session->pageTextContains('Markdown Entry Example');
  }

}
