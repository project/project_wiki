<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiPermissionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_entity_content',
    'text',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests if all permissions related to this module work.
   */
  public function testPermissions() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Try to go to the project wiki list page with the "View Project Wiki"
    // permission and check if access is denied.
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(403);
    $this->drupalLogout();
    // Try to go to the project wiki list page without the permission
    // and check if access is granted.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    // Create two test entries, one with and one without developerContent.
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(200);
    $page->fillField('edit-category-0-value', 'Category A');
    $page->fillField('edit-title-0-value', 'Title A');
    $page->fillField('edit-body-0-value', 'This entry is marked as developerContent.');
    $page->checkField('edit-isdevelopercontent-value');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(200);
    $page->fillField('edit-category-0-value', 'Category B');
    $page->fillField('edit-title-0-value', 'Title B');
    $page->fillField('edit-body-0-value', 'This entry is NOT marked as developerContent.');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Try to go to the project wiki list page with the "View Developer Content"
    // permission and check if both entries are present.
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Title A');
    $session->pageTextContains('Title B');
    // Give the regular user permission to see the wiki but not to
    // view developerContent.
    $this->drupalGet('/admin/people/permissions');
    $session->statusCodeEquals(200);
    $page->checkField('edit-authenticated-view-project-wiki');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $this->drupalLogout();
    // Try to go to the project wiki list page as the regular user
    // and check if only entry B is present.
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextNotContains('Title A');
    $session->pageTextContains('Title B');
  }

}
