<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiListjsTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'listjs',
    'listjs_cdn_alter',
    'project_wiki',
    'project_wiki_entity_content',
    'text',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    // Prepare the test page:
    $page = $this->getSession()->getPage();
    // Create three test entries that will later be searched for and sorted.
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $page->fillField('edit-category-0-value', 'Category A');
    $page->fillField('edit-title-0-value', 'Title A');
    $page->fillField('edit-body-0-value', 'The CONTENT A. This content contains 1234567890 for testing the search.');
    $page->pressButton('edit-submit');
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $page->fillField('edit-category-0-value', 'Category B');
    $page->fillField('edit-title-0-value', 'Title B');
    $page->fillField('edit-body-0-value', 'The CONTENT B.');
    $page->pressButton('edit-submit');
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $page->fillField('edit-category-0-value', 'Category C');
    $page->fillField('edit-title-0-value', 'Title C');
    $page->fillField('edit-body-0-value', 'The CONTENT C.');
    $page->checkField('edit-isdevelopercontent-value');
    $page->pressButton('edit-submit');
  }

  /**
   * Tests the search functionality of the ListJS library.
   */
  public function testListjsSearch() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to the list page:
    // Test if listjs is attached to the list correctly:
    $this->drupalGet('/admin/project-wiki');

    // Check they're there:
    $session->pageTextContains('CONTENT A');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-1'));

    $session->pageTextContains('CONTENT B');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-2'));

    $session->pageTextContains('CONTENT C');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-3'));

    // Check if searching for title works.
    $page->fillField('listjs-search', 'Title B');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-2'));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-1', 500));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-3', 500));

    // Check if searching for category works.
    $page->fillField('listjs-search', 'Category C');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-3'));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-1', 500));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-2', 500));

    // Check if searching for content works.
    $page->fillField('listjs-search', '1234567890');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-1'));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-2', 500));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-3', 500));

    // Check if searching for developer content works.
    $page->fillField('listjs-search', 'Developer Content');
    $this->assertNotEmpty($session->waitForElementVisible('css', '#project-wiki-entry-3'));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-1', 500));
    $this->assertEmpty($session->waitForElementVisible('css', '#project-wiki-entry-2', 500));
  }

  /**
   * Tests the sort functionality of the ListJS library.
   */
  public function testListjsSort() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to the list page:
    // Test if listjs is attached to the list correctly:
    $this->drupalGet('/admin/project-wiki');

    // Check they're there:
    $session->pageTextContains('CONTENT A');
    $this->assertNotNull($session->waitForElementVisible('css', '#project-wiki-entry-1'));

    $session->pageTextContains('CONTENT B');
    $this->assertNotNull($session->waitForElementVisible('css', '#project-wiki-entry-2'));

    $session->pageTextContains('CONTENT C');
    $this->assertNotNull($session->waitForElementVisible('css', '#project-wiki-entry-3'));

    // Check if sorting works.
    // @todo Implement this
  }

}
