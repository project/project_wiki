/**
 * @file
 * Project Wiki behaviors.
 */
(function (Drupal, once) {
  Drupal.behaviors.projectWikiSearch = {
    attach(context, settings) {
      once('project-wiki-search', '.project-wiki').forEach(function () {
        const options = {
          listClass: 'list-js-list',
          searchClass: 'list-js-search',
          sortClass: 'list-js-sort',
          valueNames: [
            'list-js-label',
            'list-js-category',
            'list-js-content',
            'list-js-is-dev-content',
          ],
        };
        const menuLinks = document.querySelectorAll('.project-wiki__sidebar .menu-item a');
        const entries = document.querySelectorAll('.project-wiki-entry');
        const entriesList = new List('project-wiki', options);

        // Highlight the corresponding article if an anchor link is clicked
        if (menuLinks || menuLinks.length) {
          menuLinks.forEach(menuLink => {
            menuLink.addEventListener('click', function(){
              // Reset first
              entries.forEach(el => el.classList.remove('project-wiki-entry--selected'));
              document.querySelector(menuLink.getAttribute('href')).classList.add('project-wiki-entry--selected');
            });
          });
        }
      });
    },
  };
})(Drupal, once);
