# Project Wiki

The Project Wiki module implements an editable wiki for your project in order
for you to keep track of a larger Drupal project's structure and functionality
even after some time. With it and its submodules you can easily create your
project's documentation using Drupal's UI or via
[Markdown](https://www.markdownguide.org/) files.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/project_wiki).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/project_wiki).

## Requirements

This module requires the following modules / libraries:

- [League Commonmark](https://commonmark.thephpleague.com/) (Library)
- [ListJS](https://www.drupal.org/project/listjs) (Module / Library)

Some of these requirements need to be installed via
[Composer](https://getcomposer.org/). For this, use:
~~~ shell
composer require 'bower-asset/listjs'
~~~

## Recommended modules

The Project Wiki module itself does **not provide any functionality on its
own**. Enable / disable its functions using the submodules.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

### Changing permissions:

- Navigate to _\[People] » \[Permissions]_ to edit the permissions related to
this module.

### Creating content:

In order to create wiki content, you must enable the required submodules of this
module.
