<?php

declare(strict_types=1);

namespace Drupal\project_wiki\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\project_wiki\Annotation\ProjectWikiContent;

/**
 * ProjectWikiContentPluginManager plugin manager.
 */
class ProjectWikiContentPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ProjectWikiContentPlugins', $namespaces, $module_handler, ProjectWikiContentInterface::class, ProjectWikiContent::class);
    $this->alterInfo('project_wiki_content_info');
    $this->setCacheBackend($cache_backend, 'project_wiki_content_plugins');
  }

  /**
   * Creates instances of all existing project wiki content plugins.
   */
  public function createInstances() {
    $projectWikiContentInstances = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      $projectWikiContentInstances[] = $this->createInstance($id);
    }
    return $projectWikiContentInstances;
  }

}
