<?php

declare(strict_types=1);

namespace Drupal\project_wiki\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for project_wiki_entity_content_value_object plugins.
 */
abstract class ProjectWikiContentPluginBase extends PluginBase implements ProjectWikiContentInterface {

  /**
   * {@inheritdoc}
   */
  abstract public function getValueObjects(): array;

}
