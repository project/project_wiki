<?php

namespace Drupal\project_wiki\Plugin;

/**
 * The interface employed by ProjectWikiEntityContentValueObject.
 */
interface ProjectWikiContentInterface {

  /**
   * Retrieves the plugin's value objects.
   *
   * @return ProjectWikiValueObjectInterface[]
   *   The project wiki value objects.
   */
  public function getValueObjects(): array;

}
