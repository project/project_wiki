<?php

namespace Drupal\project_wiki\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Drupal\project_wiki\Plugin\ProjectWikiContentPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Project Wiki routes.
 */
class ProjectWikiController extends ControllerBase {

  /**
   * The plugin manager instance.
   *
   * @var \Drupal\project_wiki\Plugin\ProjectWikiContentPluginManager
   */
  protected $pluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * CaptchaExamplesForm constructor.
   *
   * @param \Drupal\project_wiki\Plugin\ProjectWikiContentPluginManager $pluginManager
   *   The plugin manager instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(ProjectWikiContentPluginManager $pluginManager, ModuleHandlerInterface $moduleHandler) {
    $this->pluginManager = $pluginManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.project_wiki_content'),
      $container->get('module_handler'),
    );
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    $projectWikiContentValueObjectInstances = $this->pluginManager->createInstances();
    $result = [];

    if (!empty($projectWikiContentValueObjectInstances)) {
      foreach ($projectWikiContentValueObjectInstances as $projectWikiContentValueObjectInstance) {
        $projectWikiValueObjects = $projectWikiContentValueObjectInstance->getValueObjects();
        foreach ($projectWikiValueObjects as $projectWikiValueObject) {
          if (!$projectWikiValueObject->isDeveloperContent() || parent::currentUser()->hasPermission('view project wiki developer content')) {
            $result[] = $projectWikiValueObject;
          }
        }
      }
    }

    // Filter out any empty entries through (e.g. when no entries exist):
    $result = array_filter($result);

    $this->moduleHandler()->alter('project_wiki_entity_content_value_objects', $result);

    if (empty($result)) {
      if ($this->moduleHandler->moduleExists('help')) {
        return [
          '#type' => 'markup',
          '#markup' => $this->t('<strong>There are currently no entries in the Project Wiki.</strong><br>If you need advice on how to create wiki entries, visit the <a href=":link">help page</a>.', [
            ':link' => Url::fromRoute('help.page', ['name' => 'project_wiki'])->toString(),
          ]),
        ];
      }
      else {
        return [
          '#type' => 'markup',
          '#markup' => $this->t('<strong>There are currently no entries in the Project Wiki.</strong><br>If you need advice on how to create wiki entries, take a look at the <code>README</code> files or visit the <a href="https://www.drupal.org/project/project_wiki">project page</a>.'),
        ];
      }
    }

    $build['list'] = [
      '#theme' => 'project_wiki_list',
      '#projectWikiEntries' => $result,
      '#attached' => [
        'library' => [
          'project_wiki/global',
          'project_wiki/search',
        ],
      ],
    ];
    return $build;
  }

}
