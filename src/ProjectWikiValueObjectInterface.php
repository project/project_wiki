<?php

namespace Drupal\project_wiki;

/**
 * Interface of ProjectWikiValueObject.
 */
interface ProjectWikiValueObjectInterface {

  /**
   * Returns the provider attribute.
   */
  public function getProvider();

  /**
   * Returns the id attribute.
   */
  public function getId();

  /**
   * Returns the langcode attribute.
   */
  public function getLangcode();

  /**
   * Returns the category attribute.
   */
  public function getCategory();

  /**
   * Returns the title attribute.
   */
  public function getTitle();

  /**
   * Returns the content attribute.
   */
  public function getContent();

  /**
   * Returns the developer content attribute.
   */
  public function isDeveloperContent();

}
