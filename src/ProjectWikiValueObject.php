<?php

namespace Drupal\project_wiki;

/**
 * The value object class for project wiki contents.
 */
class ProjectWikiValueObject implements ProjectWikiValueObjectInterface {

  /**
   * The provider attribute.
   *
   * @var string
   */
  private $provider = NULL;

  /**
   * The id attribute.
   *
   * @var int
   */
  private $id;

  /**
   * The langcode attribute.
   *
   * @var string
   */
  private $langcode;

  /**
   * The category attribute.
   *
   * @var string
   */
  private $category;

  /**
   * The title attribute.
   *
   * @var string
   */
  private $title;

  /**
   * The content attribute.
   *
   * @var string
   */
  private $content;

  /**
   * The developer content attribute.
   *
   * @var bool
   */
  private $isDeveloperContent;

  /**
   * Create a new ProjectWikiEntityContentValueObject.
   *
   * The constructor that creates a new
   * ProjectWikiEntityContentValueObject.
   *
   * @param string $provider
   *   The provider value.
   * @param int $id
   *   The id value.
   * @param string $langcode
   *   The langcode value.
   * @param string $category
   *   The category value.
   * @param string $title
   *   The title value.
   * @param string $content
   *   The content value.
   * @param bool $isDeveloperContent
   *   The developer content value.
   */
  public function __construct($provider, $id, $langcode, $category, $title, $content, $isDeveloperContent) {
    $this->provider = $provider;
    $this->id = $id;
    $this->langcode = $langcode;
    $this->category = $category;
    $this->title = $title;
    $this->content = $content;
    $this->isDeveloperContent = $isDeveloperContent;
  }

  /**
   * Returns the provider attribute.
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * Returns the id attribute.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Returns the langcode attribute.
   */
  public function getLangcode() {
    return $this->langcode;
  }

  /**
   * Returns the category attribute.
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * Returns the title attribute.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Returns the content attribute.
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * Returns the developer content attribute.
   */
  public function isDeveloperContent() {
    return $this->isDeveloperContent;
  }

}
