<?php

namespace Drupal\project_wiki\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the ProjectWikiContent annotation object.
 *
 * @Annotation
 */
class ProjectWikiContent extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

}
