# Project Wiki Entity Content

As a submodule of [Project Wiki](https://www.drupal.org/project/project_wiki),
this module allows you to create, edit and delete Project Wiki entries directly
inside Drupal's UI.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/project_wiki).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/project_wiki).

## Requirements

This module requires the following modules / libraries:

- [League Commonmark](https://commonmark.thephpleague.com/) (Library)
- [ListJS](https://www.drupal.org/project/listjs) (Module / Library)
- [Project Wiki](https://www.drupal.org/project/project_wiki) (Module)

Some of these requirements need to be installed via
[Composer](https://getcomposer.org/). For this, use:
~~~ shell
composer require 'bower-asset/listjs'
~~~

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

### Creating / Managing Entity Contents:

- Navigate to _\[Content] » \[Project Wiki Entity]_
  to create, edit and delete the entity contents.

### Managing Entity Content Fields / Display:

- Navigate to _\[Structure] » \[Manage Project Wiki Entity]_ to
  manage fields, form and view display of entity contents.

### Changing Permissions:

- Navigate to _\[People] » \[Permissions]_ to edit the permissions related to
this module.
