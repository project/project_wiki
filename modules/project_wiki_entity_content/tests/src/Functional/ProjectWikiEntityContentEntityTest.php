<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiEntityContentEntityTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_entity_content',
    'project_wiki_markdown_content',
    'text',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if creating / deleting entities works.
   */
  public function testEntityCreationUi() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to the entity creation page and create a test entity.
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(200);
    $page->hasField('edit-category-0-value');
    $page->hasField('edit-title-0-value');
    $page->hasField('edit-body-0-value');
    $page->hasUncheckedField('edit-isdevelopercontent-value');
    $page->fillField('edit-category-0-value', 'Test Category');
    $page->fillField('edit-title-0-value', 'Test Title');
    $page->fillField('edit-body-0-value', 'Test Content');
    $page->checkField('edit-isdevelopercontent-value');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Go to the project wiki list page and check if the entity's there.
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Test Category');
    $session->pageTextContains('Test Title');
    $session->pageTextContains('Test Content');
    $session->pageTextContains('Developer Content');
    // Go to the entity delete page and delete it.
    $this->drupalGet('/admin/project-wiki-entity-content/1/delete');
    $session->statusCodeEquals(200);
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Go to the entity's full display page and check that the entity's gone.
    $this->drupalGet('/admin/project-wiki-entity-content/1');
    $session->statusCodeEquals(404);
    // Go to the project wiki list page and check that the entity's gone.
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextNotContains('Test Category');
    $session->pageTextNotContains('Test Title');
    $session->pageTextNotContains('Test Content');
  }

  /**
   * Tests if creating an entity without a category works.
   */
  public function testCreateEntityWithoutCategoryUi() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to the entity creation page and create a test entity, without a
    // category:
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $page->fillField('edit-title-0-value', 'Test Title');
    $page->fillField('edit-body-0-value', 'Test Content');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Go to the project wiki list page and check if the entity's there.
    $this->drupalGet('/admin/project-wiki');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Test Title');
    $session->pageTextContains('Test Content');
  }

}
