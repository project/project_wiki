<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiEntityContentPermissionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_entity_content',
    'project_wiki_markdown_content',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
  }

  /**
   * Tests if all permissions related to this module work.
   */
  public function testPermissions() {
    $session = $this->assertSession();
    // Try to go to the entity creation page as an unauthorized user
    // and check if access is denied.
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(403);
    $this->drupalLogout();
    // Try to go to the entity creation page as an authorized user
    // and check if access is granted.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/project-wiki-entity-content/add');
    $session->statusCodeEquals(200);
  }

}
