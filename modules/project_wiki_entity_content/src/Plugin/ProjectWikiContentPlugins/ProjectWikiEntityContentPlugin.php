<?php

namespace Drupal\project_wiki_entity_content\Plugin\ProjectWikiContentPlugins;

use Drupal\project_wiki\Plugin\ProjectWikiContentPluginBase;

/**
 * Plugin implementation of the project_wiki_entity_content_value_object.
 *
 * @ProjectWikiContent(
 *   id = "project_wiki_entity_content_plugin",
 * )
 */
class ProjectWikiEntityContentPlugin extends ProjectWikiContentPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValueObjects(): array {
    $contentEntities = \Drupal::entityTypeManager()->getStorage('project_wiki_entity_content')->loadMultiple();
    $projectWikiEntries = [];
    foreach ($contentEntities as $contentEntity) {
      /** @var \Drupal\project_wiki_entity_content\Entity\ProjectWikiEntityContent $contentEntity */
      $projectWikiEntries[] = $contentEntity->toValueObject();
    }
    return $projectWikiEntries;
  }

}
