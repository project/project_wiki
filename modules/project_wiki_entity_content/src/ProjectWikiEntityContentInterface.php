<?php

namespace Drupal\project_wiki_entity_content;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a project wiki entity content entity type.
 */
interface ProjectWikiEntityContentInterface extends ContentEntityInterface {

}
