<?php

namespace Drupal\project_wiki_entity_content\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\project_wiki\ProjectWikiValueObject;
use Drupal\project_wiki\ProjectWikiValueObjectInterface;
use Drupal\project_wiki_entity_content\ProjectWikiEntityContentInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the project wiki entity content entity class.
 *
 * @ContentEntityType(
 *   id = "project_wiki_entity_content",
 *   label = @Translation("Project Wiki Entity Content"),
 *   label_collection = @Translation("Project Wiki Entity Contents"),
 *   label_singular = @Translation("project wiki entity content"),
 *   label_plural = @Translation("project wiki entity contents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count project wiki entity content",
 *     plural = "@count project wiki entity contents",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\project_wiki_entity_content\ProjectWikiEntityContentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\project_wiki_entity_content\Form\ProjectWikiEntityContentForm",
 *       "edit" = "Drupal\project_wiki_entity_content\Form\ProjectWikiEntityContentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "project_wiki_entity_content",
 *   admin_permission = "administer project_wiki_entity_contents",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "body" = "body",
 *     "owner" = "owner",
 *     "status" = "status",
 *     "langcode" = "langcode"
 *   },
 *   links = {
 *     "collection" = "/admin/project-wiki-entity-content",
 *     "add-form" = "/admin/project-wiki-entity-content/add",
 *     "canonical" = "/admin/project-wiki-entity-content/{project_wiki_entity_content}",
 *     "edit-form" = "/admin/project-wiki-entity-content/{project_wiki_entity_content}/edit",
 *     "delete-form" = "/admin/project-wiki-entity-content/{project_wiki_entity_content}/delete",
 *     "delete-multiple-form" = "/admin/project-wiki-entity-content/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.project_wiki_entity_content.manage",
 * )
 */
final class ProjectWikiEntityContent extends ContentEntityBase implements ProjectWikiEntityContentInterface {
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['category'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Category'))
      ->setDescription(t('The category that this entry fits in (leave blank for no category).'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setSetting('max_length', 64)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ]);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of this entry.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Body'))
      ->setDescription(t('The body of this project wiki content.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
      ]);

    $fields['isDeveloperContent'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Developer Content'))
      ->setDescription(t('Check if this entry is developer content. Regular users of your webpage will not be able to see developer content.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'region' => 'hidden',
      ]);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'inline',
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the project wiki entity content was created.'))
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'label' => 'inline',
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last Changed'))
      ->setDescription(t('The time that the project wiki entity content was last edited.'))
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'label' => 'inline',
      ]);
    return $fields;
  }

  /**
   * Converts the BaseFieldDefinitions to a WikiEntry object.
   */
  public function toValueObject(): ProjectWikiValueObjectInterface {
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('project_wiki_entity_content');
    $content = $view_builder->view($this);

    return new ProjectWikiValueObject('project_wiki_entity_content', $this->get('id')->value, $this->get('langcode')->value, $this->get('category')->value, $this->get('title')->value, $content, $this->get('isDeveloperContent')->value);
  }

}
