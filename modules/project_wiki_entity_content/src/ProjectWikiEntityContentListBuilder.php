<?php

namespace Drupal\project_wiki_entity_content;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the project wiki entity content entity type.
 */
final class ProjectWikiEntityContentListBuilder extends EntityListBuilder {
  use EntityOwnerTrait;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['category'] = $this->t('Category');
    $header['isDeveloperContent'] = $this->t('Developer Content');
    $header['status'] = $this->t('Status');
    $header['author'] = $this->t('Author');
    $header['createdOn'] = $this->t('Creation Date');
    $header['lastModified'] = $this->t('Last Modified');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\project_wiki_entity_content\Entity\ProjectWikiEntityContent $entity */
    $row['id'] = $entity->id();
    $row['title'] = $entity->toLink();
    $row['category'] = $entity->get('category')->value;
    $row['isDeveloperContent'] = $entity->get('isDeveloperContent')->value ? $this->t('Yes') : $this->t('No');
    $row['status'] = $entity->get('status')->value;
    $row['author'] = $entity->getOwner()->value;
    $row['createdOn'] = $this->dateFormatter->format($entity->get('created')->value, 'short');
    $row['lastModified'] = $this->dateFormatter->format($entity->get('changed')->value, 'short');
    return $row + parent::buildRow($entity);
  }

}
