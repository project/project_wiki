# Project Wiki Markdown Content Example

An example module, on how to populate the
[Project Wiki](https://www.drupal.org/project/project_wiki) with
[Markdown](https://www.markdownguide.org/) data. You can view the test
Markdown entries provided with this module on the wiki page
(`/admin/project_wiki`).

## Requirements

This module requires the following modules / libraries:

- [League Commonmark](https://commonmark.thephpleague.com/) (Library)
- [ListJS](https://www.drupal.org/project/listjs) (Module / Library)
- [Project Wiki](https://www.drupal.org/project/project_wiki) (Module)
- Project Wiki Markdown Content (Module)

Some of these requirements need to be installed via
[Composer](https://getcomposer.org/). For this, use:
~~~ shell
composer require 'bower-asset/listjs'
~~~

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
