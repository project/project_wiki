---
title: 'Setting up a Module'
isDeveloperContent: TRUE
---

To set up your Markdown module to your needs, perform the following steps:

1. Copy this example module and change its folder name to your module name
   (e.g. `'my_module'`).
2. Change the filename of `'project_wiki_markdown_content_example.info.yml'` to
   your module name followed by `'.info.yml'` (e.g. `'my_module.info.yml'`).
3. Go into the file and change the `name`, `description`, and `package` values
   to your needs.
4. Change the filename of `'ProjectWikiMarkdownContentExamplePlugin.php'` to the
   name of your Module followed by `'Plugin.php'` (e.g. `'MyModulePlugin.php'`).
5. Go into the file and change the following values:
   5.1. _In line 2:_ Change `'project_wiki_markdown_content_example'` within the
   namespace to your module name (e.g. `'my_module'`).
   5.2. _In line 11:_ Change `'project_wiki_markdown_content_example_plugin'`
   within the doc comment to your module name followed by `'_plugin'`
   (e.g. `'my_module_plugin'`).
   5.3. _In line 14:_ Change the classname
   `'ProjectWikiMarkdownContentExamplePlugin'`
   to the name of your Module followed by `'Plugin'` (e.g. `'MyModulePlugin'`).
   5.4. _In line 20:_ Change `'project_wiki_markdown_content_example'` within
   the function to your module name (e.g. `'my_module'`).
   5.5. _In line 21:_ Change the `'/Examples'` folder path to the folder path
   that your Markdown entries will reside in (e.g. `'/Content/MyContent'`).
6. Insert your Markdown entries according to the _"Markdown Example"_ entry
   guidelines.
