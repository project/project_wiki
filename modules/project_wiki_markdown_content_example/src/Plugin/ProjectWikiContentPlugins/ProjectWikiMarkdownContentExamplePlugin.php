<?php

namespace Drupal\project_wiki_markdown_content_example\Plugin\ProjectWikiContentPlugins;

use Drupal\project_wiki_markdown_content\Plugin\ProjectWikiMarkdownContentPluginBase;

/**
 * The plugin that provides the Markdown Entries to the Project Wiki.
 *
 * @ProjectWikiContent(
 *   id = "project_wiki_markdown_content_example_plugin",
 * )
 */
class ProjectWikiMarkdownContentExamplePlugin extends ProjectWikiMarkdownContentPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getEntriesDirectoryPath() {
    $module_path = $this->moduleHandler->getModule('project_wiki_markdown_content_example')->getPath();
    return $module_path . "/project_wiki_markdown_content";
  }

}
