# Project Wiki Markdown Content

As a submodule of [Project Wiki](https://www.drupal.org/project/project_wiki)
this module allows you to globally
administer entries of your Project Wiki using
[Markdown](https://www.markdownguide.org/) files.

## Requirements

This module requires the following modules / libraries:

- [League Commonmark](https://commonmark.thephpleague.com/) (Library)
- [ListJS](https://www.drupal.org/project/listjs) (Module / Library)
- [Project Wiki](https://www.drupal.org/project/project_wiki) (Module)

Some of these requirements need to be installed via
[Composer](https://getcomposer.org/). For this, use:
~~~ shell
composer require 'bower-asset/listjs'
~~~

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

### Changing Permissions:

- Navigate to _\[People] » \[Permissions]_ to edit the permissions related to
this module.

### Changing Settings:

- Navigate to _\[Configuration] » \[Project Wiki settings]
  » \[Markdown Content Settings]_
  to edit settings related to Markdown content.

## FAQ

**Q: I have HTML in my Markdown file, but it doesn't show up (correctly) on the page.**  
**A:** Check in the Markdown Content Settings if "HTML filtering method"
is set to "Escape" or "Strip", which will
either show HTML code as plain text or delete it alltogether.
Note, that if you set this setting to "Allow"
you need to make sure that only people you trust add Markdown files to
prevent potential security risks.

**Q: The link in my Markdown file doesn't work on the page.**  
**A:** In this case, the actual problem may
be that **your link is unsafe** and is therefore not displayed on the page.
In that case, it is recommended to remove that link from the file.
