<?php

declare(strict_types=1);

namespace Drupal\project_wiki_markdown_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Project Wiki Markdown Content settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'project_wiki_markdown_content_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['project_wiki_markdown_content.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['filterFormat'] = [
      '#type' => 'select',
      '#options' => $this->getFilterFormatOptions(),
      '#title' => $this->t('HTML filter format'),
      '#description' => $this->t('The filter format to use for filtering the markdown content. Further selectable filter formats can be created on the <a href=@link>Text formats and editors</a> page.', ['@link' => Url::fromRoute('filter.admin_overview')->toString()]),
      '#default_value' => $this->config('project_wiki_markdown_content.settings')->get('filterFormat'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves all enabled filter formats as an options array.
   */
  protected function getFilterFormatOptions() {
    $formats = filter_formats();
    $options = [];
    foreach ($formats as $format) {
      $options[$format->id()] = $format->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('project_wiki_markdown_content.settings')
      ->set('filterFormat', $form_state->getValue('filterFormat'))
      ->save();
    // @todo Find the correct caches to clear here.
    parent::submitForm($form, $form_state);
  }

}
