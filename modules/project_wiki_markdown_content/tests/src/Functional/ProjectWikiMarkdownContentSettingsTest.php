<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiMarkdownContentSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_entity_content',
    'project_wiki_markdown_content',
    'filter_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the functionality of the settings page.
   */
  public function testSettingsChange() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to the settings page and change the settings.
    $this->drupalGet('/admin/structure/project_wiki/project_wiki_markdown_content_settings');
    $session->statusCodeEquals(200);
    $session->fieldValueEquals('filterFormat', 'filtered_html');
    $page->selectFieldOption('filterFormat', 'plain_text');
    // Save the settings and check if they are still there.
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->fieldValueEquals('filterFormat', 'plain_text');
  }

}
