<?php

namespace Drupal\Tests\project_wiki\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group project_wiki_markdown_content
 */
class ProjectWikiMarkdownContentInstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'project_wiki',
    'project_wiki_markdown_content',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the installation / uninstallation of the module.
   */
  public function testInstallUninstall() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Since the module is currently installed, check if the settings are there.
    $this->drupalGet('/admin/structure/project_wiki/project_wiki_markdown_content_settings');
    $session->statusCodeEquals(200);
    // Go to the Extend page and check if the module is marked as installed.
    $this->drupalGet('/admin/modules');
    $page->hasCheckedField('edit-modules-project-wiki-markdown-content-enable');
    // Go to the Uninstall page and uninstall the module.
    $this->drupalGet('/admin/modules/uninstall');
    $page->hasUncheckedField('edit-modules-project-wiki-markdown-content-enable');
    $page->checkField('edit-uninstall-project-wiki-markdown-content');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $this->drupalGet('/admin/modules/uninstall/confirm');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Go to the Extend page and check if the module is marked as uninstalled.
    $this->drupalGet('/admin/modules');
    $page->hasUncheckedField('edit-modules-project-wiki-markdown-content-enable');
    // Since the module is now uninstalled, check that the settings are gone.
    $this->drupalGet('/admin/structure/project_wiki/project_wiki_markdown_content_settings');
    $session->statusCodeEquals(404);
    // Go to the Extend page and check if the module is marked as uninstalled.
    $this->drupalGet('/admin/modules');
    $page->hasUncheckedField('edit-modules-project-wiki-markdown-content-enable');
    // Re-install the module and check if it is now marked as installed.
    $page->checkField('edit-modules-project-wiki-markdown-content-enable');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $page->hasCheckedField('edit-modules-project-wiki-markdown-content-enable');
    // Since the module is installed again, check if the settings are there.
    $this->drupalGet('/admin/structure/project_wiki/project_wiki_markdown_content_settings');
    $session->statusCodeEquals(200);
  }

}
